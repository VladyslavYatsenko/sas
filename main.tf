variable "region" {
  default = "eu-west-2"
}

provider "aws" {
  access_key = "AKIA4ESNZVXFMZQLA2U3"
  secret_key = "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"
  region = var.region
}

resource "aws_instance" "linux_instance" {
  ami = "ami-04a26b5fb882a2eb2"
  instance_type = "t2.micro"
  key_name = "SAS_KEY"
  vpc_security_group_ids = [
    aws_security_group.Terraform_CoreOS_Server.id
    ]

  user_data = file("app-cloud-config.yaml")

  tags = {
    Name = "linux instance"
  }
}

resource "aws_security_group" "Terraform_CoreOS_Server" {
  name = "CoreOs Server Terraform Security Group"
  ingress {
    #incoming
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = -1
    protocol = "icmp"
    to_port = -1
  }
  ingress {
    #incoming
    from_port = 8060
    protocol = "tcp"
    to_port = 8060
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 19531
    protocol = "tcp"
    to_port = 19531
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 9001
    protocol = "tcp"
    to_port = 9001
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    from_port = 9000
    protocol = "tcp"
    to_port = 9000
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  ingress {
    #incoming
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = [
      "0.0.0.0/0"]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}